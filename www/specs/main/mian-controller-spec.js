describe('mian-controller', function() {

    var ctrl,
        $rootScope;

    beforeEach(function() {
        angular.mock.module('myApp');
    });

    beforeEach(inject(function($controller, _$rootScope_) {

        ctrl = $controller('mainController');
        $rootScope = _$rootScope_;
    }));

   it('should changed showContent from false to ture', function() {

       var expectListUpdate = [
           {
               name: 'Event',
               showContent: true
           }
       ];

       ctrl.listUpdate = [
           {
               name: 'Event',
               showContent: false
           }
       ];

       ctrl.showContent(0);
       expect(ctrl.listUpdate).toEqual(expectListUpdate);

    });

 });
