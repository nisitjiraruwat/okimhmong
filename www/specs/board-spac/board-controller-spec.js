describe('board-controller', function() {

    var ctrl,
        $rootScope;

    beforeEach(function() {
        angular.mock.module('myApp');
    });

    beforeEach(inject(function($controller, _$rootScope_) {

        ctrl = $controller('boardController');
        $rootScope = _$rootScope_;
    }));

    it('should changed flag of board from false to ture', function() {

       var expectBoards = [
           {
               flag: true
           }
       ];

       ctrl.boards = [
           {
               flag: false
           }
       ];

       ctrl.saveFlag(0);
       expect(ctrl.boards).toEqual(expectBoards);

    });

    it('should changed flag of board from true to false', function() {

        var expectBoards = [
            {
                flag: false
            }
        ];

        ctrl.boards = [
            {
                flag: true
            }
        ];

        ctrl.saveFlag(0);
        expect(ctrl.boards).toEqual(expectBoards);

    });

    it('should changed flag of favoriteFromMe from false to ture', function() {

        var expectBoards = [
        {
            favoriteFromMe: true,
            favorite: 1
        }
    ];

        ctrl.boards = [
            {
                favoriteFromMe: false,
                favorite: 0
            }
        ];

        ctrl.saveFavorite(0);
        expect(ctrl.boards).toEqual(expectBoards);

    });

    it('should changed flag of board favoriteFromMe true to false', function() {

        var expectBoards = [
            {
                favoriteFromMe: false,
                favorite: 3
            }
        ];

        ctrl.boards = [
            {
                favoriteFromMe: true,
                favorite: 4
            }
        ];

        ctrl.saveFavorite(0);
        expect(ctrl.boards).toEqual(expectBoards);

    });

 });
