var babelify = require('babelify');
var browserify = require('browserify');
var gulp = require('gulp');
var Server = require('karma').Server;
var source = require('vinyl-source-stream');
var less = require('gulp-less');
var concat = require('gulp-concat');
var sequence = require('gulp-sequence');
var glob = require('glob');
var watchify = require('watchify');
var watch = require('gulp-watch');
var browserifyInc = require('browserify-incremental');
var gutil = require('gulp-util');

function jsBundle(options) {
  return browserify(options.index)
    .transform(babelify)
    .bundle()
    .pipe(source(options.output.file))
    .pipe(gulp.dest(options.output.dir));
};

gulp.task('js', function() {
  return jsBundle({
    index: 'js/app.js',
    output: {
      file: 'main.js',
      dir: 'dist/js/'
    }
  });
});

gulp.task('less', function () {
  return gulp.src(['./less/**/*.less'])
    .pipe(less())
    .pipe(concat('main.css'))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('copy-html', function() {
    return gulp.src('./template/**/*.html').pipe(gulp.dest('./dist/'));
});

gulp.task('copy-icons', function() {
    return gulp.src(['./icons/**/*.svg','./icons/**/*.ico']).pipe(gulp.dest('./dist/icons/'));
});

gulp.task('copy-css', function () {
    return gulp.src([
        'node_modules/angular-material/angular-material.css',
        'node_modules/angular-material/modules/closure/select/select.css'
    ])
        .pipe(concat('angular-material.css'))
        .pipe(gulp.dest('./dist/css/'));
});

/*
    *
    * Watch
    *
    */

function scripts(src, filename, dest, watch) {
    var bundler, rebundle;
    bundler = browserify(src, {
        basedir: __dirname,
        debug: true,
        cache: {}, // required for watchify
        packageCache: {}, // required for watchify
        fullPaths: true, // required to be true only for watchify
        transform: [babelify]
    });

    browserifyInc(bundler, {cacheFile: './browserify-cache.json'});

    if (watch) {
        bundler = watchify(bundler)
    }

    rebundle = function () {
        var stream = bundler.bundle();
        stream.on('error', function (err) {
            console.error(err.message);
            this.emit('end');
        });
        stream = stream.pipe(source(filename));
        return stream.pipe(gulp.dest(dest));
    };

    bundler.on('update', rebundle);

    bundler.on('update', function (msg) {
        gutil.log('File Changed:\n' + gutil.colors.yellow(msg.join('\n')));
    });

    bundler.on('log', function (msg) {
        gutil.log(gutil.colors.green(filename + ' is compiled, ') + msg);
    });

    return rebundle();
}

gulp.task('watch:less', function () {
    watch('./less/**/*.less', {verbose: true}, function () {
        gulp.start('less');
    });
});

gulp.task('watch:js', function () {
    glob('specs/**/*.js', function (_, files) {
        files = files.map(function (f) {
            return './' + f;
        });
        return scripts(files, 'spec.js', './build.tmp/', true);
    });
    return scripts('./js/app.js', 'main.js', './dist/js/', true);
});

gulp.task('watch:html', function () {
    watch('./template/**/*.html', {verbose: true}, function () {
        gulp.start('copy-html');
    });
});


/*
    *
    * Test
    *
    */

gulp.task('test', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('tdd', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js'
  }, done).start();
});

gulp.task('spec', function() {
    return glob('specs/**/*.js', function (_, files) {
        files = files.map(function (f) {
            return './' + f;
        });
        return jsBundle({
            index: files,
            output: {
                file: 'spec.js',
                dir: 'build.tmp/'
            }
        });
    });
});

/*
    * build
    */

gulp.task('watch', ['watch:less', 'watch:js', 'watch:html']);
gulp.task('watch:test', sequence('watch:js', 'tdd'));

gulp.task('build', ['js', 'less', 'copy-html', 'copy-icons', 'copy-css']);
