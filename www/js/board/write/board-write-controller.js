import {toHTML} from './../to-html';

let boardWriteController = ['$log', '$mdDialog', 'BoardWrite',
    function($log, $mdDialog, BoardWrite){

        $log.log('[boardWriteController] Start.');

        let ctrl = this;

        ctrl.title = '';
        ctrl.text = '';

        ctrl.showExample = (title, text) => {
            $mdDialog.show({
                controller: 'exampleBoardPageController',
                controllerAs: 'ctrl',
                templateUrl: 'example-board-page-dialog.html',
                parent: angular.element(document.body),
                locals: {
                    title: title,
                    text: text
                },
                clickOutsideToClose: true
            })
            .then(function() {
            }, function() {
            });
        };

        $log.log('[boardWriteController] End.');

    }];

export {
    boardWriteController
}
