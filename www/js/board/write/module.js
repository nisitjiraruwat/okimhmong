import angular from 'angular';

let boardWrite = angular.module('board.write', []);

import {BoardWrite} from './board-write-service';
boardWrite.factory('BoardWrite', BoardWrite);

import {boardWriteController} from './board-write-controller';
boardWrite.controller('boardWriteController', boardWriteController);
import {exampleBoardPageController} from './example-board-page-controller';
boardWrite.controller('exampleBoardPageController', exampleBoardPageController);
