let toHtmlDirective = ['$compile', '$parse', function ($compile, $parse) {
    return {
        priority: 101, // compiles first
        restrict: 'A',
        link: function(scope, element, attrs) {
            let model = $parse(attrs.toHtml);
            //var modelSetter = model.assign;
            let value = $parse(model)(scope);
            element.html(value);
            element[0].removeAttribute("to-html");
            $compile(element)(scope);
        }
    };
}];

export {toHtmlDirective};
