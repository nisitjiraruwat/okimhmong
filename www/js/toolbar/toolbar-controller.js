let toolbarController = ['$log', '$mdSidenav',
    function($log, $mdSidenav){

        $log.log('[toolbarController] Start.');

        let ctrl = this;

        ctrl.showMenu = () => {
            $mdSidenav('menu')
              .toggle();
        };

        $log.log('[toolbarController] End.');

    }];

export {
    toolbarController
}
