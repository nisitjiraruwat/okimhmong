import angular from 'angular';

let toolbar = angular.module('toolbar', []);

import {toolbarController} from './toolbar-controller';
toolbar.controller('toolbarController', toolbarController);
